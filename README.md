# openmw_connectedFilterChests

## Requires OpenMW 0.49 or above

## Youtube Demonstration
https://youtu.be/OXebcRu7Y_s

## Installation
--todo

## To Use:
1. Fill a chest with exactly 1 item (of stack size 1. I may fix this later as its annoying.)
2. Hold sneak and activate the chest to link it to the storage system, and give it a filter type
3. Hold sneak and activate chest again to unlink it
4. Hold run on a linked chests to filter items from your inventory, into the chest

# Note
If you unlink all chest for a given type, you lose access to the fake chest behind it
so make sure to have an item of that item type, so you can link another chest to retrieve your things!

	
## Known Bugs:
None at present

## License
--TODO: say how it is licensed.

