local ui = require('openmw.ui')
local self = require('openmw.self')
local core = require('openmw.core')
local types = require('openmw.types')
local input = require('openmw.input')
local async = require('openmw.async')
local I = require('openmw.interfaces')
local aux_util = require('openmw_aux.util')
local Controls = require('openmw.interfaces').Controls


local wasSneakPressed = false
local wasRunPressed = false

local function event_notify(msg)
	print(tostring(msg))
	ui.showMessage(tostring(msg))
end

local function event_open_inventory(data)
	--open chest, open player inventory
	if data.remoteContainer == nil then print("event_open_inventory data was nil") end 
	I.UI.addMode('Container', {target = data.remoteContainer})
end

local function onUpdate(dt)
	if input.isActionPressed(input.ACTION.Sneak) and wasSneakPressed == false then	
		core.sendGlobalEvent("event_sneaking", {playerName = self.type.record(self).name, sneaking = true})
		wasSneakPressed = true
	end

	if not input.isActionPressed(input.ACTION.Sneak) and wasSneakPressed == true then	
		core.sendGlobalEvent("event_sneaking", {playerName = self.type.record(self).name, sneaking = false})
		wasSneakPressed = false
	end

	if input.isActionPressed(input.ACTION.Run) and wasRunPressed == false then	
		core.sendGlobalEvent("event_running", {playerName = self.type.record(self).name, running = true})
		wasRunPressed = true
	end

	if not input.isActionPressed(input.ACTION.Run) and wasRunPressed == true then	
		core.sendGlobalEvent("event_running", {playerName = self.type.record(self).name, running = false})
		wasRunPressed = false
	end
end

return {
	engineHandlers = { 
		onLoad = onLoad, 
		onSave = onSave, 
		onUpdate = onUpdate,
		onKeyRelease = onKeyRelease
	},
	eventHandlers = {
		event_notify = event_notify,
		event_open_inventory = event_open_inventory
	} 
}

