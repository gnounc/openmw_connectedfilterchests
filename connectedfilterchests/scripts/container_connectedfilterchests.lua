local self = require('openmw.self')
local core = require('openmw.core')
local types = require('openmw.types')
local async = require('openmw.async')

if self.cell == "toddtest" then print("toddtesting out.") return end

core.sendGlobalEvent("event_requestHandler", self)


return { 
	engineHandlers = { 
		onInit = onInit, 
		onUpdate = onUpdate,
		onActivated = onActivated,
	},
	eventHandlers = {
	}
}