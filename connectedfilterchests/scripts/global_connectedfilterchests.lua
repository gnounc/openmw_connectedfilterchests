local util = require('openmw.util')
local core = require('openmw.core')
local types = require('openmw.types')
local async = require('openmw.async')
local world = require('openmw.world')
--local aux = require('openmw_aux.core')
local storage = require('openmw.storage')
local Activation = require('openmw.interfaces').Activation
local Controls = require('openmw.interfaces').Controls 

local running = {}
local sneaking = {}
local registeredContainers = {}

--todo, add saving of settings

--create a new container when you register a container with a new type
--local remoteChest = world.createObject('enchanted_chest', 1)
--enchanted_chest_weapon


local connectedChestSettings = storage.globalSection('openmw_connectedFilterChests')


local function event_running(data)
	print("running")
	running[data.playerName] = data.running
end

local function event_sneaking(data)
	sneaking[data.playerName] = data.sneaking
end

local function getTypeByString(itemType)
	return types[itemType]
end

local function deactivateOverride(chest, actor)
	return false
end

local function isHomogenous(chest, player)
	local allItems = types.Container.inventory(chest):getAll()

	if allItems == nil or #allItems < 1 then 
		player:sendEvent("event_notify", "Please 1 item to container.")
		player:sendEvent("event_notify", "Cannot set filter.")

		return false
	end

	local firstItem = allItems[1]
	local firstTypeItems = {}

	firstTypeItems = types.Container.inventory(chest):getAll(firstItem.type)

	if #allItems ~= #firstTypeItems then
		player:sendEvent("event_notify", "Please remove Items until there is only 1 item type in the container.")
		player:sendEvent("event_notify", "Cannot set filter.")

		return false
	end

	player:sendEvent("event_notify", "Setting Filter.")
	return true
end

local function toggleRegisterContainer(chest, actor)
	if isHomogenous(chest, actor) == true and registeredContainers[tostring(chest.id)] == nil then
		local typeId = types.Container.inventory(chest):getAll()[1].type
		registeredContainers[tostring(chest.id)] = tostring(typeId)	--i'm wondering if chest.id is changing??
	else
		registeredContainers[tostring(chest.id)] = nil
	end
end

local function getRemoteChest(itemType)
	local iType = string.lower(itemType)

	local containers = world.getCellByName("toddtest"):getAll(types.Container)
	for _, container in pairs(containers) do
		if container.recordId == "enchanted_chest_" .. iType then
			return container
		end
	end

	--container did not exist.
	--shove them all into the exact same space, who cares
	local remoteChest = world.createObject("enchanted_chest" .. "_" .. iType, 1)
	remoteChest:teleport("toddtest", util.vector3(2035.569580078125, 3245.40771484375, -152.9093017578125))

	return remoteChest
end

local function filterInventory(actor, itemType)
	local iType = getTypeByString(itemType)
	local remoteChest = getRemoteChest(itemType)
	local filteredItems = types.Actor.inventory(actor):getAll(iType)

	for _, item in pairs(filteredItems) do
		item:moveInto(remoteChest)
	end

--	play pickup sound first
--	local sound = aux.getPickupSound(filteredItems[1])
--	core.sound.playSound3d(sound, actor)

--	then after a delay, playe drop sound
--async:newUnsavableSimulationTimer(0.2, function()  
--		local sound = aux.getDropSound(filteredItems[1])
--		core.sound.playSound3d(sound, actor)
--end)

end

local function activationOverride(chest, actor)
	if sneaking[actor.type.record(actor).name] == true then
		toggleRegisterContainer(chest, actor)

		return false
	end

	--container isnt registered to be connected, so just do the normal thing.
	local isRegistered = registeredContainers[tostring(chest.id)] ~= nil
	if not isRegistered then return true end

	local allItems = types.Container.inventory(chest):getAll()
	local containers = world.getCellByName("toddtest"):getAll(types.Container)
	local itemType = registeredContainers[tostring(chest.id)]

	--if run is held, filter specified item type from players inventory into filtered chest
	if running[actor.type.record(actor).name] == true then
		filterInventory(actor, itemType)
		actor:sendEvent("event_notify", "filtering inventory.")
		return false
	end

	--if no chest is registered, search for next empty one and register it.
--
	--find chest by recordId, if chest does not exist, create it.
	local remoteChest = getRemoteChest(itemType)

	if #allItems > 0 then
		allItems[1]:moveInto(types.Container.inventory(remoteChest))
	end

	actor:sendEvent("event_open_inventory", {remoteContainer = remoteChest})--i think just activating it didnt work last time.
	return false
end


local function event_requestHandler(container)
	Activation.addHandlerForObject(container, activationOverride)
end

local function onSave()
	local settings = creatureStorage:asTable()["settings"]
end

local function onLoad()
end


local function onLoad()
	settings = connectedChestSettings:asTable()["settings"]

	if settings.setting_running ~= nil then
		running = settings.setting_running
	else
		print("setting_running was nil")
	end
	if settings.setting_sneaking ~= nil then
		sneaking = settings.setting_sneaking
	end
	if settings.setting_registeredContainers ~= nil then
		registeredContainers = settings.setting_registeredContainers
	end
end

local function onSave()
	local settings = {
		setting_running = running, 
		setting_sneaking = sneaking, 
		setting_registeredContainers = registeredContainers,
	}

	connectedChestSettings:set("settings", settings)
end


return { 
	engineHandlers = { 
		onSave = onSave,
		onLoad = onLoad,
		onActivate = onActivate,
		onPlayerAdded = onPlayerAdded 
	},
	eventHandlers = {
		event_running = event_running,
		event_sneaking = event_sneaking,
		event_requestHandler = event_requestHandler,
	}
}